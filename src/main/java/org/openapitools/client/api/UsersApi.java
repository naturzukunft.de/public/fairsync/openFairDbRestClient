/*
 * OpenFairDB API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.9.3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.api;

import org.openapitools.client.ApiCallback;
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.ApiResponse;
import org.openapitools.client.Configuration;
import org.openapitools.client.Pair;
import org.openapitools.client.ProgressRequestBody;
import org.openapitools.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;


import org.openapitools.client.model.InlineObject;
import org.openapitools.client.model.InlineObject1;
import org.openapitools.client.model.InlineObject2;
import org.openapitools.client.model.JwtToken;
import org.openapitools.client.model.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsersApi {
    private ApiClient localVarApiClient;

    public UsersApi() {
        this(Configuration.getDefaultApiClient());
    }

    public UsersApi(ApiClient apiClient) {
        this.localVarApiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return localVarApiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.localVarApiClient = apiClient;
    }

    /**
     * Build call for loginPost
     * @param inlineObject  (required)
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response - the JWT token </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call loginPostCall(InlineObject inlineObject, final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = inlineObject;

        // create path and map variables
        String localVarPath = "/login";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, String> localVarCookieParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] {  };
        return localVarApiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call loginPostValidateBeforeCall(InlineObject inlineObject, final ApiCallback _callback) throws ApiException {
        
        // verify the required parameter 'inlineObject' is set
        if (inlineObject == null) {
            throw new ApiException("Missing the required parameter 'inlineObject' when calling loginPost(Async)");
        }
        

        okhttp3.Call localVarCall = loginPostCall(inlineObject, _callback);
        return localVarCall;

    }

    /**
     * User login
     * 
     * @param inlineObject  (required)
     * @return JwtToken
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response - the JWT token </td><td>  -  </td></tr>
     </table>
     */
    public JwtToken loginPost(InlineObject inlineObject) throws ApiException {
        ApiResponse<JwtToken> localVarResp = loginPostWithHttpInfo(inlineObject);
        return localVarResp.getData();
    }

    /**
     * User login
     * 
     * @param inlineObject  (required)
     * @return ApiResponse&lt;JwtToken&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response - the JWT token </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<JwtToken> loginPostWithHttpInfo(InlineObject inlineObject) throws ApiException {
        okhttp3.Call localVarCall = loginPostValidateBeforeCall(inlineObject, null);
        Type localVarReturnType = new TypeToken<JwtToken>(){}.getType();
        return localVarApiClient.execute(localVarCall, localVarReturnType);
    }

    /**
     * User login (asynchronously)
     * 
     * @param inlineObject  (required)
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response - the JWT token </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call loginPostAsync(InlineObject inlineObject, final ApiCallback<JwtToken> _callback) throws ApiException {

        okhttp3.Call localVarCall = loginPostValidateBeforeCall(inlineObject, _callback);
        Type localVarReturnType = new TypeToken<JwtToken>(){}.getType();
        localVarApiClient.executeAsync(localVarCall, localVarReturnType, _callback);
        return localVarCall;
    }
    /**
     * Build call for logoutPost
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call logoutPostCall(final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/logout";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, String> localVarCookieParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] { "jwtAuth" };
        return localVarApiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call logoutPostValidateBeforeCall(final ApiCallback _callback) throws ApiException {
        

        okhttp3.Call localVarCall = logoutPostCall(_callback);
        return localVarCall;

    }

    /**
     * User logout
     * 
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public void logoutPost() throws ApiException {
        logoutPostWithHttpInfo();
    }

    /**
     * User logout
     * 
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<Void> logoutPostWithHttpInfo() throws ApiException {
        okhttp3.Call localVarCall = logoutPostValidateBeforeCall(null);
        return localVarApiClient.execute(localVarCall);
    }

    /**
     * User logout (asynchronously)
     * 
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call logoutPostAsync(final ApiCallback<Void> _callback) throws ApiException {

        okhttp3.Call localVarCall = logoutPostValidateBeforeCall(_callback);
        localVarApiClient.executeAsync(localVarCall, _callback);
        return localVarCall;
    }
    /**
     * Build call for usersCurrentGet
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> The current user </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call usersCurrentGetCall(final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/users/current";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, String> localVarCookieParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] { "jwtAuth" };
        return localVarApiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call usersCurrentGetValidateBeforeCall(final ApiCallback _callback) throws ApiException {
        

        okhttp3.Call localVarCall = usersCurrentGetCall(_callback);
        return localVarCall;

    }

    /**
     * Get the current user
     * 
     * @return User
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> The current user </td><td>  -  </td></tr>
     </table>
     */
    public User usersCurrentGet() throws ApiException {
        ApiResponse<User> localVarResp = usersCurrentGetWithHttpInfo();
        return localVarResp.getData();
    }

    /**
     * Get the current user
     * 
     * @return ApiResponse&lt;User&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> The current user </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<User> usersCurrentGetWithHttpInfo() throws ApiException {
        okhttp3.Call localVarCall = usersCurrentGetValidateBeforeCall(null);
        Type localVarReturnType = new TypeToken<User>(){}.getType();
        return localVarApiClient.execute(localVarCall, localVarReturnType);
    }

    /**
     * Get the current user (asynchronously)
     * 
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> The current user </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call usersCurrentGetAsync(final ApiCallback<User> _callback) throws ApiException {

        okhttp3.Call localVarCall = usersCurrentGetValidateBeforeCall(_callback);
        Type localVarReturnType = new TypeToken<User>(){}.getType();
        localVarApiClient.executeAsync(localVarCall, localVarReturnType, _callback);
        return localVarCall;
    }
    /**
     * Build call for usersResetPasswordPost
     * @param inlineObject2  (required)
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call usersResetPasswordPostCall(InlineObject2 inlineObject2, final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = inlineObject2;

        // create path and map variables
        String localVarPath = "/users/reset-password";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, String> localVarCookieParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] {  };
        return localVarApiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call usersResetPasswordPostValidateBeforeCall(InlineObject2 inlineObject2, final ApiCallback _callback) throws ApiException {
        
        // verify the required parameter 'inlineObject2' is set
        if (inlineObject2 == null) {
            throw new ApiException("Missing the required parameter 'inlineObject2' when calling usersResetPasswordPost(Async)");
        }
        

        okhttp3.Call localVarCall = usersResetPasswordPostCall(inlineObject2, _callback);
        return localVarCall;

    }

    /**
     * Request a users password
     * 
     * @param inlineObject2  (required)
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public void usersResetPasswordPost(InlineObject2 inlineObject2) throws ApiException {
        usersResetPasswordPostWithHttpInfo(inlineObject2);
    }

    /**
     * Request a users password
     * 
     * @param inlineObject2  (required)
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<Void> usersResetPasswordPostWithHttpInfo(InlineObject2 inlineObject2) throws ApiException {
        okhttp3.Call localVarCall = usersResetPasswordPostValidateBeforeCall(inlineObject2, null);
        return localVarApiClient.execute(localVarCall);
    }

    /**
     * Request a users password (asynchronously)
     * 
     * @param inlineObject2  (required)
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call usersResetPasswordPostAsync(InlineObject2 inlineObject2, final ApiCallback<Void> _callback) throws ApiException {

        okhttp3.Call localVarCall = usersResetPasswordPostValidateBeforeCall(inlineObject2, _callback);
        localVarApiClient.executeAsync(localVarCall, _callback);
        return localVarCall;
    }
    /**
     * Build call for usersResetPasswordRequestPost
     * @param inlineObject1  (required)
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call usersResetPasswordRequestPostCall(InlineObject1 inlineObject1, final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = inlineObject1;

        // create path and map variables
        String localVarPath = "/users/reset-password-request";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, String> localVarCookieParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] {  };
        return localVarApiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call usersResetPasswordRequestPostValidateBeforeCall(InlineObject1 inlineObject1, final ApiCallback _callback) throws ApiException {
        
        // verify the required parameter 'inlineObject1' is set
        if (inlineObject1 == null) {
            throw new ApiException("Missing the required parameter 'inlineObject1' when calling usersResetPasswordRequestPost(Async)");
        }
        

        okhttp3.Call localVarCall = usersResetPasswordRequestPostCall(inlineObject1, _callback);
        return localVarCall;

    }

    /**
     * Request a password reset
     * 
     * @param inlineObject1  (required)
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public void usersResetPasswordRequestPost(InlineObject1 inlineObject1) throws ApiException {
        usersResetPasswordRequestPostWithHttpInfo(inlineObject1);
    }

    /**
     * Request a password reset
     * 
     * @param inlineObject1  (required)
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<Void> usersResetPasswordRequestPostWithHttpInfo(InlineObject1 inlineObject1) throws ApiException {
        okhttp3.Call localVarCall = usersResetPasswordRequestPostValidateBeforeCall(inlineObject1, null);
        return localVarApiClient.execute(localVarCall);
    }

    /**
     * Request a password reset (asynchronously)
     * 
     * @param inlineObject1  (required)
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Sucessful response </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call usersResetPasswordRequestPostAsync(InlineObject1 inlineObject1, final ApiCallback<Void> _callback) throws ApiException {

        okhttp3.Call localVarCall = usersResetPasswordRequestPostValidateBeforeCall(inlineObject1, _callback);
        localVarApiClient.executeAsync(localVarCall, _callback);
        return localVarCall;
    }
}
